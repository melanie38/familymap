package com.example.melanie.familymap_2.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.example.melanie.familymap_2.Child;
import com.example.melanie.familymap_2.Models.Event;
import com.example.melanie.familymap_2.Models.Person;
import com.example.melanie.familymap_2.R;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class PersonActivity extends AppCompatActivity {

    private TextView firstName;
    private TextView lastName;
    private TextView gender;

    private String personId;
    private Person person;

    private ArrayList<Person> allPersons;
    private ArrayList<Event> allEvents;
    private RecyclerView personRecycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        allPersons = MainActivity.getAllPersons();
        allEvents = MainActivity.getAllEvents();

        firstName = (TextView) findViewById(R.id.first_name);
        lastName = (TextView) findViewById(R.id.last_name);
        gender = (TextView) findViewById((R.id.gender));

        if (getIntent().hasExtra("person")) {
            personId = getIntent().getStringExtra("person");
        }

        updatePerson();

        PersonAdapter mPersonAdapter = new PersonAdapter(this, generateLists());
        mPersonAdapter.setCustomParentAnimationViewId(R.id.parent_list_item_expand_arrow);
        mPersonAdapter.setParentClickableViewAnimationDefaultDuration();
        mPersonAdapter.setParentAndIconExpandOnClick(true);

        personRecycleView = (RecyclerView) findViewById(R.id.recycler_view);

        personRecycleView.setLayoutManager(new LinearLayoutManager(this));

        personRecycleView.setAdapter(mPersonAdapter);

    }

    public void updatePerson() {

        for (Person individual : allPersons) {
            if (individual.getPersonId().equals(personId)) {
                person = individual;
            }
        }
        firstName.setText(person.getFirstName());
        lastName.setText(person.getLastName());
        if (person.getGender().equals("f")) {
            gender.setText("Female");
        }
        else {
            gender.setText("Male");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_map_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // switch statement on the item
        // back button finish();
        // top button takes me back to the map fragment

        Intent intent;
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.back_arrow:
                intent = new Intent(getBaseContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    public class ViewHolderParent extends ParentViewHolder {

        TextView title;

        public ViewHolderParent(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.parent_title);
        }

    }

    public class ViewHolderChild extends ChildViewHolder {

        TextView top;
        TextView bottom;
        ImageView icon;
        String type;
        String id;

        public ViewHolderChild(View itemView) {
            super(itemView);

            top = (TextView) itemView.findViewById(R.id.top);
            bottom = (TextView) itemView.findViewById(R.id.bottom);
            icon = (ImageView) itemView.findViewById(R.id.icon);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (type.equals("person")) {
                        Intent i = new Intent(PersonActivity.this, PersonActivity.class);
                        i.putExtra(type, id);
                        startActivity(i);
                    }
                    else {
                        Intent i = new Intent(PersonActivity.this, MapsActivity.class);
                        i.putExtra(type, id);
                        startActivity(i);
                    }
                }
            });

        }

    }

    public class Parent implements ParentObject {

        /* Create an instance variable for your list of children */
        private List<Object> mChildrenList;
        String parentTitle;

        /**
         * Your constructor and any other accessor
         *  methods should go here.
         */

        @Override
        public List<Object> getChildObjectList() {
            return mChildrenList;
        }

        @Override
        public void setChildObjectList(List<Object> list) {
            mChildrenList = list;
        }
    }

    public class PersonAdapter extends ExpandableRecyclerAdapter<ViewHolderParent, ViewHolderChild> {
    //public class PersonAdapter extends RecyclerAdapter<SearchViewHolder> {

        LayoutInflater mInflater;

        public PersonAdapter(Context context, List<ParentObject> parentItemList) {
            super(context, parentItemList);
            mInflater = LayoutInflater.from(context);
        }
// onCreateViewHolder, onBindViewHolder
        @Override
        public ViewHolderParent onCreateParentViewHolder(ViewGroup viewGroup) {

            View view = mInflater.inflate(R.layout.view_holder_parent, viewGroup, false);
            return new ViewHolderParent(view);
        }

        @Override
        public ViewHolderChild onCreateChildViewHolder(ViewGroup viewGroup) {
            View view = mInflater.inflate(R.layout.view_holder_child, viewGroup, false);
            return new ViewHolderChild(view);
        }

        @Override
        public void onBindParentViewHolder(ViewHolderParent viewHolderParent, int i, Object o) {
            Parent parent = (Parent) o;
            viewHolderParent.title.setText(parent.parentTitle);
        }

        @Override
        public void onBindChildViewHolder(ViewHolderChild viewHolderChild, int i, Object o) {
            final Child child = (Child) o;
            viewHolderChild.bottom.setText(child.getBottom().toString());
            viewHolderChild.top.setText(child.getTop().toString());
            viewHolderChild.id = child.getId();
            viewHolderChild.type = child.getIsPerson();

            if (child.getType().equals("m")) {
                viewHolderChild.icon.setImageResource(R.drawable.blue);
            }
            else if (child.getType().equals("f")) {
                viewHolderChild.icon.setImageResource(R.drawable.pink);
            }
            else if (child.getType().equals("event")) {
                viewHolderChild.icon.setImageResource(R.drawable.arrow_up);
            }

        }
    }

    private ArrayList<ParentObject> generateLists() {

        ArrayList<ParentObject> parentObjects = new ArrayList<>();

        ArrayList<Person> relatives = person.getRelatives(); // get the list from the person
        ArrayList<Object> childList = new ArrayList<>();

        for (Person person : relatives) {
            String top = person.getFirstName() + " " + person.getLastName();
            String bottom = person.getType();
            childList.add(new Child(top, bottom, person.getGender(), person.getPersonId(), "person"));
        }

        Parent family = new Parent();
        family.parentTitle = "FAMILY";
        family.setChildObjectList(childList);

        parentObjects.add(family);

        ArrayList<Event> events = person.getRelatedEvents();
        childList = new ArrayList<>();

        for (Event event : events) {
            String top = event.getDescription() + ":" + event.getCity() + ", " + event.getCountry() + " (" + event.getYear() + ")";
            String bottom = person.getFirstName() + " " + person.getLastName();
            childList.add(new Child(top, bottom, "event", event.getEventID(), "event"));
        }

        Parent lifeEvents = new Parent();
        lifeEvents.parentTitle = "LIFE EVENTS";
        lifeEvents.setChildObjectList(childList);

        parentObjects.add(lifeEvents);

        return parentObjects;
    }
}
