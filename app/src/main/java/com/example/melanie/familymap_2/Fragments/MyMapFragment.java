package com.example.melanie.familymap_2.Fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.melanie.familymap_2.Activities.FilterActivity;
import com.example.melanie.familymap_2.Activities.MapsActivity;
import com.example.melanie.familymap_2.Activities.SearchActivity;
import com.example.melanie.familymap_2.Activities.SettingActivity;
import com.example.melanie.familymap_2.Models.Event;
import com.example.melanie.familymap_2.Activities.MainActivity;
import com.example.melanie.familymap_2.Models.Person;
import com.example.melanie.familymap_2.Activities.PersonActivity;
import com.example.melanie.familymap_2.Models.User;
import com.example.melanie.familymap_2.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class MyMapFragment extends Fragment implements OnMapReadyCallback{

    private GoogleMap mMap;
    private SupportMapFragment mMapView;
    private TextView textView;
    private ImageView imageView;
    private Person person;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_mymap, container, false);
        //mMapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.fragment_mymap);

        if (mMapView == null) {
            mMapView = new SupportMapFragment();
        }
        mMapView.getMapAsync(this);

        getChildFragmentManager().beginTransaction().add(R.id.fragment_mymap, mMapView).commit();

        textView = (TextView) view.findViewById(R.id.textView);
        imageView = (ImageView) view.findViewById(R.id.imageView);

        return view;
    }
/*
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch(item.getItemId()) {
            case R.id.searchMenuItem:
                intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
                break;
            case R.id.searchMenuFilter:
                intent = new Intent(getActivity(), FilterActivity.class);
                startActivity(intent);
                break;
            case R.id.searchMenuSettings:
                intent = new Intent(getActivity(), SettingActivity.class);
                startActivity(intent);
                break;
            default:
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getActivity() instanceof MainActivity){
            inflater.inflate(R.menu.menu, menu);
        }
        else {
            inflater.inflate(R.menu.activity_map_menu, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera


        placeMarkers(mMap);
        addRelatives();

        String eventId = MapsActivity.getEventId();
        ArrayList<Event> allEvents = MainActivity.getAllEvents();

        for (Event event : allEvents) {
            if (event.getEventID().equals(eventId)) {
                LatLng eventPosition = new LatLng(Double.parseDouble(event.getLatitude()), Double.parseDouble(event.getLongitude()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(eventPosition));
            }
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener(){
            @Override
            public boolean onMarkerClick(Marker marker){
                // create a new text view in the XML
                updateInformation(marker);

                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // start activity
                        Intent i = new Intent(getActivity(), PersonActivity.class);
                        i.putExtra("person", person.getPersonId());
                        startActivity(i);
                    }
                });

                return false;
            }

        });

        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    public void updateInformation(Marker marker) {
        ArrayList<Event> allEvents = MainActivity.getAllEvents();
        ArrayList<Person> allPersons = MainActivity.getAllPersons();

        String personId = null;
        String firstName = null;
        String lastName = null;
        String eventType = null;
        String city = null;
        String country = null;
        String year = null;
        String gender = null;

        for (Event event : allEvents) {
            if (Double.parseDouble(event.getLatitude()) == marker.getPosition().latitude &&
                    Double.parseDouble(event.getLongitude()) == marker.getPosition().longitude) {

                for (Person person : allPersons) {
                    if (person.getPersonId().equals(event.getPersonID())) {
                        personId = person.getPersonId();
                        firstName = person.getFirstName();
                        lastName = person.getLastName();
                        gender = person.getGender();
                    }
                }

                eventType = event.getDescription();
                city = event.getCity();
                country = event.getCountry();
                year = event.getYear();

                break;
            }
        }

        textView.setText(
                firstName + " " + lastName + "\n" +
                eventType + ": " + city + ", " + country + " (" + year + ")" );

        if (gender.equals("m")) {
            imageView.setImageResource(R.drawable.blue);
        }
        else {
            imageView.setImageResource(R.drawable.pink);
        }

        person = new Person(personId, firstName, lastName, gender);

    }

    public void placeMarkers(GoogleMap mMap) {

        ArrayList<Event> allEvents = MainActivity.getAllEvents();

        for (Event event : allEvents) {
            Person p = findPersonById(event.getPersonID());
            if (event.getShow() && p.getShow()) {
                LatLng llevent = new LatLng(Double.parseDouble(event.getLatitude()), Double.parseDouble(event.getLongitude()));
                mMap.addMarker(new MarkerOptions()
                        .position(llevent)
                        .icon(colorMarkers(event.getDescription())));
            }
        }
    }

    public Person findPersonById(String personId) {
        for (Person p : MainActivity.getAllPersons()) {
            if (p.getPersonId().equals(personId)) {
                return p;
            }
        }
        return null;
    }

    public BitmapDescriptor colorMarkers(String eventType) {

        BitmapDescriptor color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET);

        switch (eventType) {
            case "birth" : color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE); break;
            case "death" : color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED); break;
            case "christening" : color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN); break;
            case "baptism" : color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN); break;
            case "census" : color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW); break;
            case "marriage" : color = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE); break;
        }

        return color;
    }

    public void relatedEvents(Person p) {

        String personId = p.getPersonId();

        ArrayList<Event> allEvents = MainActivity.getAllEvents();
        ArrayList<Event> personEvents = new ArrayList<>();

        for (Event event : allEvents) {
            if (personId.equals(event.getPersonID())) {
                personEvents.add(event);
            }
        }

        p.setRelatedEvents(personEvents);
    }

    public void addRelatives() {
        ArrayList<Person> allPersons = MainActivity.getAllPersons();
        //HashMap<String, Person> allRelatives = new HashMap<>();

        for (Person person : allPersons) {

            relatedEvents(person);

            ArrayList<Person> personRelatives = new ArrayList<>();

            if (person.getSpouse() != null) {
                for (Person spouse : allPersons) {
                    if (person.getSpouse().equals(spouse.getPersonId())) {
                        spouse.setType("SPOUSE");
                        Person familyMember = new Person(spouse);
                        personRelatives.add(familyMember);
                        break;
                    }
                }
            }
            if (person.getFather() != null) {
                for (Person father : allPersons) {
                    if (person.getFather().equals(father.getPersonId())) {
                        father.setType("FATHER");
                        Person familyMember = new Person(father);
                        person.setType("CHILD");
                        Person child = new Person(person);
                        father.getRelatives().add(child);
                        personRelatives.add(familyMember);
                        break;
                    }
                }
            }
            if (person.getMother() != null) {
                for (Person mother : allPersons) {
                    if (person.getMother().equals(mother.getPersonId())) {
                        mother.setType("MOTHER");
                        Person familyMember = new Person(mother);
                        person.setType("CHILD");
                        Person child = new Person(person);
                        mother.getRelatives().add(child);
                        personRelatives.add(familyMember);
                        break;
                    }
                }
            }

            person.setRelatives(personRelatives);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap != null) {
            mMap.clear();
            placeMarkers(mMap);
        }
        //clear the map and redraw all my pins
    }
}
