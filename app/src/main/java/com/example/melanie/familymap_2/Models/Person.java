package com.example.melanie.familymap_2.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

/**
 * Created by Melanie on 04/08/16.
 */
public class Person {

    private String descendant;
    private String personId;
    private String firstName;
    private String lastName;
    private String gender;
    private String spouse;
    private String father;
    private String mother;
    private String type; // spouse, father, mother, child
    private Boolean show;

    // add a list of relatives id with thier type
    private ArrayList<Person> relatives;
    // add a list of events related to the person
    private ArrayList<Event> relatedEvents;


    public Person(String descendant, String personId, String firstName, String lastName, String gender, String spouse, String father, String mother) {
        this.descendant = descendant;
        this.personId = personId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.spouse = spouse;
        this.father = father;
        this.mother = mother;

        show = true;

        relatives = new ArrayList<>();
        relatedEvents = new ArrayList<>();
    }

    public Person(String personId, String firstName, String lastName, String gender) {
        this.personId = personId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;

        show = true;

        relatives = new ArrayList<>();
        relatedEvents = new ArrayList<>();
    }

    public Person(Person p) {
        personId = p.personId;
        firstName = p.firstName;
        lastName = p.lastName;
        gender = p.gender;
        type = p.type;
    }

    public Person(){}


    public void setDescendant(String descendant) { this.descendant = descendant; }

    public void setPersonId(String personID) { this.personId = personID; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public void setGender(String gender) { this.gender = gender; }

    public void setSpouse(String spouse) { this.spouse = spouse; }

    public void setFather(String father) { this.father = father; }

    public void setMother(String mother) { this.mother = mother; }

    public void setType(String type) { this.type = type; }

    public void setRelatives(ArrayList<Person> relatives) { this.relatives = relatives; }

    public void setRelatedEvents(ArrayList<Event> relatedEvents) { this.relatedEvents = relatedEvents; }

    public String getPersonId() { return personId; }

    public String getDescendant() { return descendant; }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public String getGender() { return gender; }

    public String getSpouse() { return spouse; }

    public String getFather() { return father; }

    public String getMother() { return mother; }

    public String getType() { return type; }

    public ArrayList<Person> getRelatives() { return relatives; }

    public ArrayList<Event> getRelatedEvents() { return relatedEvents; }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public static String makeString(Reader reader) throws IOException {

        StringBuilder sb = new StringBuilder();
        char[] buf = new char[512];

        int n = 0;
        while ((n = reader.read(buf)) > 0) {
            sb.append(buf, 0, n);
        }

        return sb.toString();
    }

}
