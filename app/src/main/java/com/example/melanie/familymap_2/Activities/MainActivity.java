package com.example.melanie.familymap_2.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.example.melanie.familymap_2.Models.Event;
import com.example.melanie.familymap_2.Fragments.LoginFragment;
import com.example.melanie.familymap_2.Models.Person;
import com.example.melanie.familymap_2.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static ArrayList<Person> allPersons;
    private static ArrayList<Event> allEvents;
    public static Boolean isLogin = true;

    public MainActivity(){}

    public static void setAllPersons(ArrayList<Person> setOfPersons) { allPersons = setOfPersons; }
    public static void setAllEvents(ArrayList<Event> setOfEvents) { allEvents = setOfEvents; }

    public static ArrayList<Event> getAllEvents() {return allEvents;}
    public static ArrayList<Person> getAllPersons() {return allPersons;}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);

        if (isLogin) {
            fragment = new LoginFragment();
            fm.beginTransaction()
                    .add(R.id.fragmentContainer, fragment)
                    .commit();
        }
        else {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
