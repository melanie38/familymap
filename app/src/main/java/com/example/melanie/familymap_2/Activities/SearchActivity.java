package com.example.melanie.familymap_2.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import com.example.melanie.familymap_2.Child;
import com.example.melanie.familymap_2.Models.Event;
import com.example.melanie.familymap_2.Models.Person;
import com.example.melanie.familymap_2.R;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView searchRecycleView;
    private ArrayList<Object> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        final TextView toSearch = (TextView) findViewById(R.id.search);

        if (toSearch != null) {
            toSearch.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

                    search(makeString(s).toLowerCase());

                    SearchAdapter mSearchAdapter = new SearchAdapter(getBaseContext(), results);
                    searchRecycleView = (RecyclerView) findViewById(R.id.recycler_view_search);
                    searchRecycleView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                    searchRecycleView.setAdapter(mSearchAdapter);
                }
            });
        }

    }

    public String makeString(CharSequence s) {
        final StringBuilder sb = new StringBuilder(s.length());
        sb.append(s);
        return sb.toString();
    }

    public ArrayList<Object> search(CharSequence search){

        ArrayList<Person> allPerson = MainActivity.getAllPersons();
        ArrayList<Event> allEvents = MainActivity.getAllEvents();
        results = new ArrayList<>();

        if (search.equals("")) {
            return results;
        }

        for (Person person : allPerson) {
            if (person.getFirstName().toLowerCase().contains(search)) {
                results.add(person);
            }
            else if (person.getLastName().toLowerCase().contains(search) && !results.contains(person)) {
                results.add(person);
            }
        }

        for (Event event : allEvents) {
            if (event.getDescription().toLowerCase().contains(search)
                    || event.getCity().toLowerCase().contains(search)
                    || event.getCountry().toLowerCase().contains(search)
                    || event.getYear().toLowerCase().contains(search)) {
                results.add(event);
            }

        }

        return results;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public class SearchAdapter extends RecyclerView.Adapter<SearchViewHolder> {
        //public class PersonAdapter extends RecyclerAdapter<SearchViewHolder> {

        LayoutInflater mInflater;


        public SearchAdapter(Context context, ArrayList<Object> list) {
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.view_holder_child, parent, false);
            return new SearchViewHolder(view);
        }

        @Override
        public void onBindViewHolder(SearchViewHolder holder, int position) {

            String top;
            String bottom = null;

            // if its a person
            if (results.get(position) instanceof Person) {
                Person p = (Person) results.get(position);
                top = p.getFirstName() + " " + p.getLastName();
                holder.top.setText(top);
                holder.bottom.setText("");
                holder.type = "person";
                holder.id = p.getPersonId();
                if (p.getGender().equals("m")) {
                    holder.icon.setImageResource(R.drawable.blue);
                }
                else {
                    holder.icon.setImageResource(R.drawable.pink);
                }
            }
            else if (results.get(position) instanceof Event) {

                ArrayList<Person> allPersons = MainActivity.getAllPersons();
                Event e = (Event) results.get(position);

                top = e.getDescription() + ": " + e.getCity() + ", " + e.getCountry() + " (" + e.getYear() + ")";
                for (Person p : allPersons) {
                    if (p.getPersonId().equals(e.getPersonID())) {
                        bottom = p.getFirstName() + " " + p.getLastName();
                    }
                }

                holder.top.setText(top);
                holder.bottom.setText(bottom);
                holder.type = "event";
                holder.id = e.getEventID();
                holder.icon.setImageResource(R.drawable.arrow_up);

            }
            // if its an event


        }

        @Override
        public int getItemCount() {
            if (results != null) {
                return results.size();
            }
            else { return 0; }
        }
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder{

        TextView top;
        TextView bottom;
        ImageView icon;
        String type;
        String id;

        public SearchViewHolder(View itemView) {
            super(itemView);

            top = (TextView) itemView.findViewById(R.id.top);
            bottom = (TextView) itemView.findViewById(R.id.bottom);
            icon = (ImageView) itemView.findViewById(R.id.icon);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (type.equals("person")) {
                        Intent i = new Intent(SearchActivity.this, PersonActivity.class);
                        i.putExtra(type, id);
                        startActivity(i);
                    }
                    else {
                        Intent i = new Intent(SearchActivity.this, MapsActivity.class);
                        i.putExtra(type, id);
                        startActivity(i);
                    }
                }
            });
        }
    }
}
