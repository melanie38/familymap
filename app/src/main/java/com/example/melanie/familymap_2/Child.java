package com.example.melanie.familymap_2;

import android.widget.ImageView;

/**
 * Created by Melanie on 30/08/16.
 */
public class Child {

    private String top;
    private String bottom;
    private String type; // to know if its a person or event
    private String id;
    private String isPerson;

    public Child(String top, String bottom, String type, String id, String isPerson) {
        this.top = top;
        this.bottom = bottom;
        this.type = type;
        this.id = id;
        this.isPerson = isPerson;
    }

    public Child(String top, String bottom, String type, String id) {
        this.top = top;
        this.bottom = bottom;
        this.type = type;
        this.id = id;
    }

    public String getTop() {
        return top;
    }

    public String getBottom() {
        return bottom;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getIsPerson() {
        return isPerson;
    }
}
