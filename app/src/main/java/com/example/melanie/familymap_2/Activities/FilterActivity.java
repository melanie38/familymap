package com.example.melanie.familymap_2.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.melanie.familymap_2.Models.Event;
import com.example.melanie.familymap_2.Models.Person;
import com.example.melanie.familymap_2.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.TreeSet;

public class FilterActivity extends AppCompatActivity {

    private ArrayList<String> allEvents;
    private RecyclerView filterRecycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        getEvents();

        FilterAdapter mFilterAdapter = new FilterAdapter(getBaseContext(), allEvents);
        filterRecycleView = (RecyclerView) findViewById(R.id.recycler_view_filter);
        filterRecycleView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        filterRecycleView.setAdapter(mFilterAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public class FilterAdapter extends RecyclerView.Adapter<FilterViewHolder> {

        LayoutInflater mInflater;

        public FilterAdapter(Context context, ArrayList<String> list) {
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.view_holder_filter, parent, false);
            return new FilterViewHolder(view);
        }

        @Override
        public void onBindViewHolder(FilterViewHolder holder, int position) {

            holder.type = allEvents.get(position);
            holder.top.setText(holder.type + " Events");
            holder.bottom.setText("FILTER BY " + holder.type.toUpperCase() + " EVENTS");

            if (holder.type.equals("male")) {
                for (Person p : MainActivity.getAllPersons()) {
                    if (p.getGender().equals("m")) {
                        holder.toSwitch.setChecked(p.getShow());
                        break;
                    }
                }
            }
            else if (holder.type.equals("female")) {
                for (Person p : MainActivity.getAllPersons()) {
                    if (p.getGender().equals("f")) {
                        holder.toSwitch.setChecked(p.getShow());
                        break;
                    }
                }
            }
            else {
                for (Event event : MainActivity.getAllEvents()) {
                    if (event.getDescription().equals(holder.type)) {
                        holder.toSwitch.setChecked(event.getShow());
                        break;
                    }
                }
            }


        }

        @Override
        public int getItemCount() {
            if (allEvents != null) {
                return allEvents.size();
            }
            else { return 0; }
        }
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder{

        TextView top;
        TextView bottom;
        Switch toSwitch;
        String type;
        String gender;

        public FilterViewHolder(View itemView) {
            super(itemView);

            top = (TextView) itemView.findViewById(R.id.event);
            bottom = (TextView) itemView.findViewById(R.id.description);
            toSwitch = (Switch) itemView.findViewById(R.id.switch1);


            if (toSwitch != null) {
                toSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            if (type.equals("male")) {
                                for (Person p : MainActivity.getAllPersons()) {
                                    if (p.getGender().equals("m")) {
                                        p.setShow(true);
                                    }
                                }
                            }
                            else if (type.equals("female")) {
                                for (Person p : MainActivity.getAllPersons()) {
                                    if (p.getGender().equals("f")) {
                                        p.setShow(true);
                                    }
                                }
                            }
                            else {
                                for (Event event : MainActivity.getAllEvents()) {
                                    if (event.getDescription().equals(type)) {
                                        event.setShow(true);
                                    }
                                }
                            }
                        } else {
                            if (type.equals("male")) {
                                for (Person p : MainActivity.getAllPersons()) {
                                    if (p.getGender().equals("m")) {
                                        p.setShow(false);
                                    }
                                }
                            }
                            else if (type.equals("female")) {
                                for (Person p : MainActivity.getAllPersons()) {
                                    if (p.getGender().equals("f")) {
                                        p.setShow(false);
                                    }
                                }
                            }
                            for (Event event : MainActivity.getAllEvents()) {
                                if (event.getDescription().equals(type)) {
                                    event.setShow(false);
                                }
                            }
                        }
                    }
                });
            }

        }
    }

    public void getEvents() {
        ArrayList<Event> toParse = MainActivity.getAllEvents();
        TreeSet<String> noDuplicate = new TreeSet<>();
        allEvents = new ArrayList<>();

        for (Event event : toParse) {
            noDuplicate.add(event.getDescription());
        }

        for (String str : noDuplicate) {
            allEvents.add(str);
        }

        allEvents.add("male");
        allEvents.add("female");
    }
}
